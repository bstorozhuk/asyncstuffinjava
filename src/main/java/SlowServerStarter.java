import com.bstorozhuk.VeryVerySlowServerProbablyWrittenInPhp;

/**
 * @author bstorozhuk
 */
public class SlowServerStarter {

    public static final int PORT = 8081;

    public static void main(String[] args) {
        VeryVerySlowServerProbablyWrittenInPhp server =
                new VeryVerySlowServerProbablyWrittenInPhp(PORT);
        server.setDebug(true);
    }
}
