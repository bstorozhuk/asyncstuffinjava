import java.util.AbstractMap.SimpleEntry;
import java.util.ArrayList;
import java.util.List;

import com.bstorozhuk.DFuture;
import com.bstorozhuk.Downloader;

import static java.lang.System.currentTimeMillis;
import static java.text.MessageFormat.format;

/**
 * @author bstorozhuk
 */
public class DownloadManager {
    public static void main(String[] args) {
        Downloader downloader = new Downloader();


        long start = currentTimeMillis();

        List<DFuture<?>> dFutures = new ArrayList<>();

        for (int i = 0; i < 5; i++) {
            DFuture<SimpleEntry<String, Integer>> finalFuture = downloader.download()
                    .thenApply(message -> message + " additional text")
                    .thenApply(message -> {
                        SimpleEntry<String, Integer> entry = new SimpleEntry<>(message, message.length());
                        System.out.println(entry);
                        return entry;
                    });
            dFutures.add(finalFuture);
        }

        DFuture<Void> allTasksFinished = DFuture.allOf(dFutures);
        allTasksFinished
                .then(() -> System.out.println(format("Finished in {0} sec.", (currentTimeMillis() - start) / 1000L)));

        downloader.start();
    }
}
