package com.bstorozhuk;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.ServerSocketChannel;
import java.nio.channels.SocketChannel;
import java.nio.channels.WritableByteChannel;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadFactory;

/**
 * @author bstorozhuk
 */
public class VeryVerySlowServerProbablyWrittenInPhp {

    public static final long SLEEP_TIME_MILLIS = 1000L;
    private boolean debug = false;
    private final ExecutorService internalPool;

    public VeryVerySlowServerProbablyWrittenInPhp(int port) {
        internalPool = Executors.newFixedThreadPool(6, new MyThreadFactory());
        try {
            ServerSocketChannel serverChannel = ServerSocketChannel.open();
            serverChannel.bind(new InetSocketAddress(port));
            internalPool.submit(new InternalLoop(serverChannel, (WritableByteChannel socketChannel) -> {
                try {
                    if (debug) System.out.println("Slow server REQUEST_START");
                    Thread.sleep(SLEEP_TIME_MILLIS);
                    socketChannel.write(ByteBuffer.wrap("pong".getBytes()));
                    if (debug) System.out.println("Slow server REQUEST_FINISHED");
                } catch (InterruptedException | IOException e) {
                    throw new IllegalStateException("Can't handle connection.", e);
                }
            }));
            if (debug) System.out.println("Slow server started on port: " + port);
        } catch (IOException e) {
            throw new IllegalStateException("Can't create a server instance.", e);
        }
    }

    public boolean isDebug() {
        return debug;
    }

    public void setDebug(boolean debug) {
        this.debug = debug;
    }

    private class InternalLoop implements Runnable {

        private final ServerSocketChannel serverChannel;
        private final InternalLoopConnectionHandler handler;

        public InternalLoop(ServerSocketChannel serverChannel, InternalLoopConnectionHandler handler) {
            this.serverChannel = serverChannel;
            this.handler = handler;
        }

        @Override
        public void run() {
            if (isDebug()) System.out.println("Slow server internal loop STARTED.");
            while (true) {
                if (isDebug()) System.out.println("Slow server internal loop ITERATION.");
                try {
                    SocketChannel socketChannel = serverChannel.accept();
                    internalPool.submit(() -> handler.handleConnection(socketChannel));
                } catch (IOException e) {
                    System.out.println("Error in event loop: " + e.getMessage());
                    e.printStackTrace();
                }
            }
        }

    }

    @FunctionalInterface
    private interface InternalLoopConnectionHandler {
        void handleConnection(WritableByteChannel socketChannel);
    }

    private static class MyThreadFactory implements ThreadFactory {

        private ThreadGroup slowServerThreadGroup = new ThreadGroup("SlowServerThreadGroup_" + this.hashCode());

        @Override
        public Thread newThread(Runnable r) {
            return new Thread(slowServerThreadGroup, r);
        }
    }
}
