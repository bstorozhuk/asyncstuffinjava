package com.bstorozhuk;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.SelectionKey;
import java.nio.channels.Selector;
import java.nio.channels.SocketChannel;
import java.util.Iterator;

/**
 * @author bstorozhuk
 * async:
 * + non-blocking sockets
 * + callbacks
 * + event loop
 */
public class Downloader {

    private final Selector selector;

    public Downloader() {
        try {
            selector = Selector.open();
        } catch (IOException e) {
            throw new IllegalStateException("Can't initiate downloader.", e);
        }
    }

    public DFuture<String> download() {
        DFuture<String> future = new DFuture<>();
        try {
            SocketChannel socketChannel = SocketChannel.open();
            socketChannel.connect(new InetSocketAddress(8081));
            socketChannel.configureBlocking(false);

            System.out.println("ping");
            socketChannel.register(selector, SelectionKey.OP_READ, (Runnable) () -> {
                try {
                    ByteBuffer buffer = ByteBuffer.allocate(4);
                    socketChannel.read(buffer);
                    String message = new String(buffer.array());
                    future.complete(message);
                } catch (IOException e) {
                    throw new IllegalStateException("Can't read data from socket.", e);
                }
            });
        } catch (IOException e) {
            throw new IllegalStateException("Can't download resource.", e);
        }
        return future;
    }

    public void start() {
        while (true) {
            try {
                selector.select();

                Iterator<SelectionKey> keyIterator = selector.selectedKeys().iterator();
                while (keyIterator.hasNext()) {
                    SelectionKey key = keyIterator.next();
                    keyIterator.remove();
                    Runnable callback = (Runnable) key.attachment();
                    callback.run();
                }

            } catch (IOException e) {
                System.out.println("Error in event loop: " + e.getMessage());
            }
        }
    }
}
