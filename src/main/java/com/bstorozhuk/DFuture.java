package com.bstorozhuk;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.Function;

/**
 * @author bstorozhuk
 */
public class DFuture<T> {
    List<Function<T, ?>> dependentTasks = new ArrayList<>();

    public <R> DFuture<R> thenApply(final Function<T, R> task) {
        DFuture<R> future = new DFuture<>();
        Function<T, R> dependentTask = task.andThen(futureResult -> future.complete(futureResult));
        dependentTasks.add(dependentTask);

        return future;
    }

    public T complete(final T futureResult) {

        for (Function<T, ?> dependentTask : dependentTasks) {
            dependentTask.apply(futureResult);
        }
        return futureResult;
    }

    public DFuture<Void> then(final Runnable task) {
        return thenApply(futureResult -> {
            task.run();
            return null;
        });
    }

    public static DFuture<Void> allOf(final List<DFuture<?>> dFutures) {
        DFuture<Void> allTasksComplete = new DFuture<>();
        AtomicInteger countOfTasks = new AtomicInteger(dFutures.size());

        for (DFuture<?> dFuture : dFutures) {
            dFuture.then(() -> {
                if (countOfTasks.decrementAndGet() == 0) {
                    allTasksComplete.complete(null);
                }
            });
        }

        return allTasksComplete;
    }
}
